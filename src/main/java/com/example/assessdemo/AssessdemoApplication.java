package com.example.assessdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssessdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssessdemoApplication.class, args);
    }

}
